<?php

class book {
    public $book_id;
    public $title;
    public $authors;
    public $book_grade;
    public $is_read;


    public function __construct($title, $book_grade, $is_read, $book_id = null)
    {
        $this->book_id = $book_id;
        $this->title = $title;
        $this->book_grade = $book_grade;
        $this->is_read = $is_read;
        $this->authors = [];

    }

    public function addAuthor($author){
        $this->authors[$author->getId()] = $author;
    }
}