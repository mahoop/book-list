<?php

require_once 'php-ng-template-master/src/tpl.php';
require_once 'dao.php';
require_once 'functions.php';

//ei tööta is_read raamatutel (selle muutmine)
//lisa autorite editimine
//kuidas kuvada õiget autorit book-addis, kas saab mingi kohtadena sealt authorite arrayst?



$data = [];
//bookiga seotud
if ($_GET["cmd"] == "book-add") {
    $contentPath = 'book-add.html';
    $data['authors'] = getAuthors();

} elseif ($_GET["cmd"] == "save-book") {
    $errors = validateBook($_POST);

    if (isset($_POST["deleteButton"])) {
        deleteBookById($_POST["book_id"]);
        $data['action'] = "Deleted";
        $data['books'] = getBooks();
        $contentPath = "book-list.html";

    } elseif ($errors != []) {
        //näita uuesti formi, lisa post data $datasse
        $data["authors"] = getAuthors();
        $contentPath = "book-add.html";
        $data["errors"] = $errors;
        $book = getBookFromPost($_POST);
        $data["book"] = $book;
        $data["author2"] = getFirstAuthor($book);
        $data["author1"] = getSecondAuthor($book);
        if($data["author1"] == null){
            $data["author1"] = $data["author2"];
            $data["author2"] = null;
        }
        if (isset($_POST["edit"])) {
            $data['action'] = "edit";
        }

    } elseif (isset($_POST["edit"])) {
        $book = getBookFromPost($_POST);
        editBookById($_POST["book_id"], $book);
        header('Location: index.php?cmd=book-list&action=Changed');
        die();
//        $data['action'] = "Book changed";
//        $data['books'] = getBooks();
//        $contentPath = "book-list.html";

    } else {
        bookToDb(getBookFromPost($_POST));
        header('Location: index.php?cmd=book-list&action=Success');
        die();
//        $contentPath = "book-list.html";
//        $data['books'] = getBooks();
//        $data['action'] = "Success";
        //kui vaja panna datasse, et success
    }
} elseif ($_GET["cmd"] == "book-edit") {
    $data["authors"] = getAuthors();
    $book = getBookById($_GET["id"]);
    $data["book"] = $book;
    $data["author2"] = getFirstAuthor($book);
    $data["author1"] = getSecondAuthor($book);
    if($data["author1"] == null){
        $data["author1"] = $data["author2"];
        $data["author2"] = null;
    }
    $data["action"] = "edit";
    $contentPath = "book-add.html";


    //autoriga seotud
} elseif ($_GET["cmd"] == "author-add") {
    $contentPath = 'author-add.html';


} elseif ($_GET["cmd"] == "save-author") {
    $errors = validateAuthor($_POST);

    if (isset($_POST["deleteButton"])) {
        deleteAuthorById($_POST["author_id"]);
        $data['action'] = "Deleted";
        $data['authors'] = getAuthors();
        $contentPath = "author-list.html";

    } elseif ($errors != []) {
        //näita uuesti formi, lisa post data $datasse
        $contentPath = "author-add.html";
        $data["errors"] = $errors;
        $data["author"] = getAuthorFromPost($_POST);
        if (isset($_POST["edit"])) {
            $data['action'] = "edit";
        }

    } elseif (isset($_POST["edit"])) {
        $author = getAuthorFromPost($_POST);
        editAuthorById($_POST["author_id"], $author);
        $data['action'] = "Author changed";
        $data['authors'] = getAuthors();
        $contentPath = "author-list.html";

    } else {
        //lisa ja näita, et lisatud
        authorToDb(getAuthorFromPost($_POST));
        $contentPath = "author-list.html";
        $data['authors'] = getAuthors();
        $data['action'] = "Success";
        //kui vaja panna datasse, et success
    }
} elseif
($_GET["cmd"] == "author-edit") {
    $author = getAuthorById($_GET["id"]);
    $data["author"] = $author;
    $data["action"] = "edit";
    $contentPath = "author-add.html";

} elseif ($_GET["cmd"] == "author-list") {
    $data['authors'] = getAuthors();
    $contentPath = "author-list.html";
} else {
    $contentPath = 'book-list.html';
    $data['books'] = getBooks();
    $data['action'] = $_GET["action"];
}

$data['contentPath'] = $contentPath;
print renderTemplate('templates/main.html', $data);