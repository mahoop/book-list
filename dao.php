<?php

require_once 'book.php';
require_once 'author.php';

function getConnection()
{
    try {
        $username = 'mahoop';
        $password = '5244';
        $address = 'mysql:host=db.mkalmo.xyz;dbname=mahoop';
        $connection = new PDO($address, $username, $password);
        $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return $connection;

    } catch (\mysql_xdevapi\Exception $e) {
        echo "error while loading database";
    }
    return "";
}

function getBooks()
{
    $connection = getConnection();
    $stmt = $connection->prepare('SELECT books.book_id, books.title,
books.book_grade, books.is_read,  authors.author_id FROM books
LEFT OUTER JOIN author_in_books on books.book_id = author_in_books.book_id
LEFT OUTER JOIN authors ON author_in_books.author_id=authors.author_id;');
    $stmt->execute();
    $books = [];
    foreach ($stmt as $row) {
        if(!array_key_exists($row["book_id"], $books)){
            $books[$row["book_id"]]  = new book($row["title"], $row["book_grade"], $row["is_read"], $row["book_id"]);
        }
        if(isset($row["author_id"])){
            $books[$row["book_id"]]->addAuthor(getAuthorById($row["author_id"]));
        }
    }
    $connection = null;
    return $books;

}
function getBookById($id){
    $books = getBooks();
    return $books[$id];
}

function getAuthors()
{
    $conn = getConnection();
    $stmt = $conn->prepare('SELECT * FROM authors;');
    $stmt->execute();
    $authors = [];

    foreach ($stmt as $row){
        $author = new author($row["first_name"], $row["last_name"], $row["author_grade"], $row["author_id"]);
        $authors[$row["author_id"]]= $author;
    }

    $connection = null;
    return $authors;
}

function getAuthorById($id){
    $conn = getConnection();
    $stmt = $conn->prepare('SELECT * FROM authors WHERE author_id = ?;');
    $stmt->execute([$id]);
    $result = $stmt->fetch();
    $author = new author($result["first_name"], $result["last_name"], $result["author_grade"], $result["author_id"]);
    $connection = null;
    return $author;
}

function authorToDb($author){
    $connection = getConnection();
    $sql = "INSERT INTO authors (first_name, last_name, author_grade) VALUES (?,?,?)";
    if ($author->first_name == ""){
        $author->first_name = NULL;
    }

    if ($author->last_name==''){
        $author->last_name = NULL;
    }
    if ($author->author_grade==''){
        $author->author_grade = NULL;
    }
    $stmt = $connection->prepare($sql);
    $stmt->execute([$author->first_name, $author->last_name, $author->author_grade]);
    $connection = null;
}

function bookToDb($book){
    $connection = getConnection();
    $sql = "INSERT INTO books (title, book_grade, is_read) VALUES (?,?,?)";
    $stmt = $connection->prepare($sql);
    $stmt->execute([$book->title, $book->book_grade, $book->is_read]);
    $book_id = $connection->lastInsertId();
    $sql = "INSERT INTO author_in_books (book_id, author_id) VALUES (?,?)";
    foreach ($book->authors as $author){
        $stmt = $connection->prepare($sql);
        $stmt->execute([$book_id, $author->author_id]);
    }
    $connection = null;

}

function deleteAuthorById($author_id){
    $conn = getConnection();
    $stmt = $conn->prepare('DELETE FROM authors WHERE author_id = ?;');
    $stmt->execute([$author_id]);
    $stmt = $conn->prepare('DELETE FROM author_in_books WHERE author_id = ?;');
    $stmt->execute([$author_id]);
}

function deleteBookById($book_id){
    $conn = getConnection();
    $stmt = $conn->prepare('DELETE FROM books WHERE book_id = ?;');
    $stmt->execute([$book_id]);
    $stmt = $conn->prepare('DELETE FROM author_in_books WHERE book_id = ?;');
    $stmt->execute([$book_id]);
}

function editAuthorById($author_id, $author){
    $conn = getConnection();
    $sql = "UPDATE authors SET first_name = ?, last_name = ?, author_grade = ? WHERE author_id = ?";
    $stmt = $conn->prepare($sql);
    $stmt->execute([$author->first_name, $author->last_name, $author->author_grade, $author_id]);
    $conn = null;
}

function editBookById($book_id, $book){
    $conn = getConnection();
    $sql = "UPDATE books SET title = ?, book_grade= ?, is_read = ? WHERE book_id = ?";
    $stmt = $conn->prepare($sql);
    $stmt->execute([$book->title, $book->book_grade, $book->is_read, $book_id]);
    $stmt = $conn->prepare('DELETE FROM author_in_books WHERE book_id = ?');
    $stmt->execute([$book_id]);
    $sql = "INSERT INTO author_in_books (book_id, author_id) VALUES (?,?)";
    foreach ($book->authors as $author){
        $stmt = $conn->prepare($sql);
        $stmt->execute([$book_id, $author->author_id]);
    }
    $conn = null;
}


