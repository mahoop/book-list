<?php
//kõik funktsioonid
function validateAuthor($post)
{
    $errors = [];
    if (strlen($post["firstName"]) > 21) {
        array_push($errors, "Eesnimi liiga pikk, peab olema 1 kuni 21 märki!");
    } elseif (strlen($post["firstName"]) < 1) {
        array_push($errors, "Eesnimi liiga lühike, peab olema 1 kuni 21 märki!");
    } elseif (strlen($post["lastName"]) > 22) {
        array_push($errors, "Perenimi liiga pikk, peab olema 2 kuni 22 märki!");
    } elseif (strlen($post["lastName"]) < 2) {
        array_push($errors, "Perenimi liiga lühike, peab olema 2 kuni 22 märki!");
    }

    return $errors;
}

function getAuthorFromPost($get)
{
    if (isset($get["author_id"])) {
        return new author($get["firstName"], $get["lastName"], $get["grade"], $get["author_id"]);
    }
    return new author($get["firstName"], $get["lastName"], $get["grade"]);
}

function validateBook($post)
{
    $errors = [];
    //eesnimi, perenimi, hinne, firstName, lastName, grade
    if (strlen($post["title"]) > 23) {
        array_push($errors, "Pealkiri liiga pikk, peab olema 3 kuni 23 märki!");
    } elseif (strlen($post["title"]) < 3) {
        array_push($errors, "Pealkiri liiga lühike, peab olema 3 kuni 23 märki!");
    }
    return $errors;
}

function getBookFromPost($post)
{
    if (isset($post["book_id"])) {
        $book = new book($post["title"], $post["grade"], $post["isRead"], $post["book_id"]);
        if (isset($post["author1"])) {
            $book->addAuthor(getAuthorById($post["author1"]));
        }
        if (isset($post["author2"])) {
            $book->addAuthor(getAuthorById($post["author2"]));
        }
        return $book;
    }
    return new book($post["title"], $post["bookGrade"], $post["isRead"]);
}

function getFirstAuthor($book)
{
    foreach ($book->authors as $author) {
        if (!isset($first)) {
            return $author;
        }
    }
}

function getSecondAuthor($book)
{
    $second = null;
    foreach ($book->authors as $author) {

        if (!isset($first)) {
            var_dump($author);
            $first = $author;
            continue;
        }
        if ($second == null) {
            var_dump($author);
            $second = $author;
        }
    }
    return $second;
}

