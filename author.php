<?php

//kaustasin https://www.w3schools.com/php/php_oop_constructor.asp
class author
{
    public $author_id;
    public $first_name;
    public $last_name;
    public $author_grade;


    public function __construct($firstname, $lastname, $authorGrade, $authorId = null)
    {
        $this->first_name = $firstname;
        $this->last_name = $lastname;
        $this->author_grade = $authorGrade;
        $this->author_id = $authorId;

    }
    public function getId(){
        return $this->author_id;
    }
}